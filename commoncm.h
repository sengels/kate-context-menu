//---------------------------------------------------------------------------
// Copyright 2012-2013 Patrick Spendrin <ps_ml@gmx.de>
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
// commoncm.h
//---------------------------------------------------------------------------

//{72619BBF-9859-4015-B0B3-84D7A0363F00}

#ifndef COMMONCM_H
#define COMMONCM_H

#include <guiddef.h>
#include <Unknwn.h>
#include <shlobj.h>
#include <Shobjidl.h>
#include <string>
#include <vector>

#if defined(__MINGW32__) && !defined(MINGW_HAS_SECURE_API)
#define wcsncpy_s(dst,dstsize,src,srcsize) wcsncpy(dst,src,srcsize)
#endif

#define ResultFromShort(i) ResultFromScode(MAKE_SCODE(SEVERITY_SUCCESS, 0, (USHORT)(i)))

DEFINE_GUID(CLSID_ShellExtension,
            0xe78e8bad, 0x21f5, 0x4651, 0xbd, 0xf9, 0x05, 0x25, 0x73, 0x56, 0x9d, 0x5f);

STDAPI DllCanUnloadNow(void);
STDAPI DllGetClassObject(REFCLSID rclsid, REFIID riid, LPVOID *ppv);
STDAPI DllRegisterServer(void);
STDAPI DllUnregisterServer(void);

extern TCHAR szShellExtensionTitle[];
extern TCHAR szShellExtensionRegistryEntry[];
extern TCHAR szShellExtensionCommandString[];

class ShellExtensionFactory : public IClassFactory {
protected:
    ULONG m_cRef;

public:
    ShellExtensionFactory();
    ~ShellExtensionFactory();

    STDMETHODIMP QueryInterface(REFIID, LPVOID FAR *);
    STDMETHODIMP_(ULONG) AddRef();
    STDMETHODIMP_(ULONG) Release();
    STDMETHODIMP CreateInstance(LPUNKNOWN, REFIID, LPVOID FAR *);
    STDMETHODIMP LockServer(BOOL);
};

class ShellExtension : public IContextMenu, IShellExtInit {
public:
protected:
    ULONG m_cRef;
    UINT m_cbFiles;
    STGMEDIUM m_stgMedium;
    LPDATAOBJECT m_pDataObj;
    HKEY m_hkey;
    HBITMAP m_hKateLogoBmp;

public:
    ShellExtension();
    ~ShellExtension();

    STDMETHODIMP QueryInterface(REFIID, LPVOID FAR *);
    STDMETHODIMP_(ULONG) AddRef();
    STDMETHODIMP_(ULONG) Release();

    STDMETHODIMP QueryContextMenu(HMENU hMenu,
                                    UINT indexMenu,
                                    UINT idCmdFirst,
                                    UINT idCmdLast,
                                    UINT uFlags);

    STDMETHODIMP InvokeCommand(LPCMINVOKECOMMANDINFO lpcmi);

    STDMETHODIMP GetCommandString(UINT_PTR idCmd,
                                    UINT uFlags,
                                    UINT FAR *reserved,
                                    LPSTR pszName,
                                    UINT cchMax);

    STDMETHODIMP Initialize(LPCITEMIDLIST pIDFolder,
                            LPDATAOBJECT pDataObj,
                            HKEY hKeyID);
private:

    std::wstring getKateCommandline(std::vector<std::wstring> fileList, int session, bool openNewSession) const;

    std::vector<std::wstring> getKateSessions() const;

    static std::wstring fromNativeSeparators(const std::wstring& path);
};


#endif // COMMONCM_H