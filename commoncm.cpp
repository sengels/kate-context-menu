//---------------------------------------------------------------------------
// Copyright 2012-2013 Patrick Spendrin <ps_ml@gmx.de>
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
// commoncm.cpp
// Defines the entry point for the DLL application.
//---------------------------------------------------------------------------

#define INITGUID
#define _WIN32_WINNT 0x0600
#include "commoncm.h"
#include "resource.h"

#include <windows.h>
#include <GuidDef.h>
#include <shlguid.h>
#include <shlobj.h>

#include <sstream>

#define GUID_SIZE sizeof(GUID)

typedef struct{
  HKEY  hRootKey;
  LPTSTR szSubKey;
  LPTSTR lpszValueName;
  LPTSTR szData;
} DOREGSTRUCT;

HINSTANCE dllModule = NULL; // DLL Module.
UINT objectReferenceCounter = 0;

BOOL __stdcall RegisterServer(CLSID, LPTSTR);
BOOL __stdcall UnregisterServer(CLSID, LPTSTR);

class ShellExtensionFactory;
class ShellExtension;

BOOL __stdcall DllMain(HINSTANCE hInstance, DWORD dwReason, LPVOID lpReserved)
{
    if(dwReason == DLL_PROCESS_ATTACH) {
        dllModule = hInstance;
    }
    
    if (dwReason == DLL_PROCESS_DETACH) {
        OutputDebugString(TEXT("Unloading dll! thats not what we wanted!"));
    }

    return TRUE;
}

HRESULT __stdcall DllCanUnloadNow(void)
{
    return (objectReferenceCounter == 0 ? S_OK : S_FALSE);
}

HRESULT __stdcall DllGetClassObject(REFCLSID rclsid, REFIID riid, LPVOID *ppv)
{
    *ppv = NULL;
    if (IsEqualIID(rclsid, CLSID_ShellExtension)) {
        ShellExtensionFactory *sef = new ShellExtensionFactory;
        return sef->QueryInterface(riid, ppv);
    }
    return CLASS_E_CLASSNOTAVAILABLE;
}

//---------------------------------------------------------------------------
// DllRegisterServer
//---------------------------------------------------------------------------
HRESULT __stdcall DllRegisterServer(void)
{
    return (RegisterServer(CLSID_ShellExtension, szShellExtensionTitle) ? S_OK : E_FAIL);
}

//---------------------------------------------------------------------------
// DllUnregisterServer
//---------------------------------------------------------------------------
HRESULT __stdcall DllUnregisterServer(void)
{
    return (UnregisterServer(CLSID_ShellExtension, szShellExtensionTitle) ? S_OK : E_FAIL);
}

//---------------------------------------------------------------------------
// RegisterServer
//---------------------------------------------------------------------------
BOOL __stdcall RegisterServer(CLSID clsid, LPTSTR lpszTitle)
{
    int      i;
    HKEY     hKey;
    LRESULT  lResult;
    DWORD    dwDisp;
    TCHAR    szSubKey[MAX_PATH];
    TCHAR    szCLSID[MAX_PATH];
    TCHAR    szModule[MAX_PATH];
    LPOLESTR pwsz;

    if(FAILED(StringFromIID(clsid, &pwsz))) {
        return FALSE;
    }
    lstrcpy(szCLSID, pwsz);

    //get this app's path and file name
    std::string dllName = CM_OUTPUT_NAME ".dll";

    // it seems that for COM objects the hInstance from DllMain is wrong
    // thus we have to search for ourselves
    GetModuleFileName(GetModuleHandleA(dllName.c_str()), szModule, MAX_PATH);
    
    DOREGSTRUCT ClsidEntries[] = {
        HKEY_CLASSES_ROOT,   TEXT("CLSID\\%s"),                                                 NULL,                   lpszTitle,
        HKEY_CLASSES_ROOT,   TEXT("CLSID\\%s\\InprocServer32"),                                 NULL,                   szModule, // szModule,
        HKEY_CLASSES_ROOT,   TEXT("CLSID\\%s\\InprocServer32"),                                 TEXT("ThreadingModel"), TEXT("Apartment"),
        HKEY_CLASSES_ROOT,   TEXT("*\\shellex\\ContextMenuHandlers\\Kate"),   NULL,                   szCLSID,
        NULL,                NULL,                                                              NULL,                   NULL
    };
    
    // Register the CLSID entries
    for(i = 0; ClsidEntries[i].hRootKey; i++) {
        // Create the sub key string - for this case, insert the file extension
        wsprintf(szSubKey, ClsidEntries[i].szSubKey, szCLSID);
        lResult = RegCreateKeyEx(ClsidEntries[i].hRootKey, szSubKey, 0, NULL, REG_OPTION_NON_VOLATILE, KEY_WRITE, NULL, &hKey, &dwDisp);
        if(NOERROR == lResult) {
            TCHAR szData[MAX_PATH];
            // If necessary, create the value string
            wsprintf(szData, ClsidEntries[i].szData, szModule);
            lResult = RegSetValueEx(hKey, ClsidEntries[i].lpszValueName, 0, REG_SZ, (LPBYTE)szData, (lstrlen(szData) + 1) * sizeof(TCHAR));
            RegCloseKey(hKey);
        }
        else {
            OutputDebugString(TEXT("registering inproc server failed!"));
            return FALSE;
        }
    }
    return TRUE;
}

//---------------------------------------------------------------------------
// UnregisterServer
//---------------------------------------------------------------------------
BOOL __stdcall UnregisterServer(CLSID clsid, LPTSTR lpszTitle)
{
/*!
 * This function removes the keys from registry
 */

    TCHAR szCLSID[GUID_SIZE + 1];
    TCHAR szCLSIDKey[GUID_SIZE + 32];
    TCHAR szKeyTemp[MAX_PATH + GUID_SIZE];
    CoInitialize(NULL);
    LPOLESTR pwsz = NULL;
    if(FAILED(StringFromIID(clsid, &pwsz))) {
        return FALSE;
    }
    lstrcpy(szCLSID, pwsz);
    LPMALLOC pMalloc;
    if(FAILED(CoGetMalloc(1, &pMalloc))) {
    }
    CoUninitialize();

    lstrcpy(szCLSIDKey, TEXT("CLSID\\"));
    lstrcat(szCLSIDKey, szCLSID);

    wsprintf(szKeyTemp, TEXT("*\\shellex\\ContextMenuHandlers\\%s"), lpszTitle);
    RegDeleteKey(HKEY_CLASSES_ROOT, szKeyTemp);

    wsprintf(szKeyTemp, TEXT("%s\\%s"), szCLSIDKey, TEXT("InprocServer32"));
    RegDeleteKey(HKEY_CLASSES_ROOT, szKeyTemp);
    RegDeleteKey(HKEY_CLASSES_ROOT, szCLSIDKey);

    return TRUE;
}

//---------------------------------------------------------------------------
// ShellExtensionFactory
//---------------------------------------------------------------------------
ShellExtensionFactory::ShellExtensionFactory()
{
    m_cRef = 0L;
    objectReferenceCounter++;
}

ShellExtensionFactory::~ShellExtensionFactory()
{
    objectReferenceCounter--;
}

STDMETHODIMP ShellExtensionFactory::QueryInterface(REFIID riid, LPVOID FAR *ppv)
{
    *ppv = NULL;
    if (IsEqualIID(riid, IID_IUnknown) || IsEqualIID(riid, IID_IClassFactory)) {
        *ppv = (LPCLASSFACTORY)this;
        AddRef();
        return NOERROR;
    }
    return E_NOINTERFACE;
}

STDMETHODIMP_(ULONG) ShellExtensionFactory::AddRef()
{
    return ++m_cRef;
}

STDMETHODIMP_(ULONG) ShellExtensionFactory::Release()
{
    if (--m_cRef)
        return m_cRef;
    delete this;
    return 0L;
}

STDMETHODIMP ShellExtensionFactory::CreateInstance(LPUNKNOWN pUnkOuter, REFIID riid, LPVOID *ppvObj)
{
    *ppvObj = NULL;
    if (pUnkOuter)
        return CLASS_E_NOAGGREGATION;
    ShellExtension* pShellExt = new ShellExtension();
    if (0 == pShellExt)
        return E_OUTOFMEMORY;
    return pShellExt->QueryInterface(riid, ppvObj);
}

STDMETHODIMP ShellExtensionFactory::LockServer(BOOL fLock)
{
    return NOERROR;
}

//---------------------------------------------------------------------------
// ShellExtension
//---------------------------------------------------------------------------
ShellExtension::ShellExtension()
{
    m_cRef = 0L;
    m_pDataObj = NULL;
    objectReferenceCounter++;
    m_hKateLogoBmp = LoadBitmap(GetModuleHandleA(CM_OUTPUT_NAME ".dll"), MAKEINTRESOURCE(IDB_KATE));
    int err = GetLastError();
    if(0 != err) {
        char* msgBuf;

        FormatMessageA(FORMAT_MESSAGE_ALLOCATE_BUFFER|FORMAT_MESSAGE_FROM_SYSTEM|FORMAT_MESSAGE_IGNORE_INSERTS, NULL, err, 0,(LPSTR)&msgBuf, 0, NULL);
        OutputDebugStringA(msgBuf);
        LocalFree(msgBuf);
    }

}

ShellExtension::~ShellExtension()
{
    if (m_pDataObj)
        m_pDataObj->Release();
    objectReferenceCounter--;
    DeleteObject(m_hKateLogoBmp);
    RegCloseKey(m_hkey);
}

STDMETHODIMP ShellExtension::QueryInterface(REFIID riid, LPVOID FAR *ppv)
{
    *ppv = NULL;
    if (IsEqualIID(riid, IID_IShellExtInit) || IsEqualIID(riid, IID_IUnknown)) {
        *ppv = (LPSHELLEXTINIT)this;
    }
    else if (IsEqualIID(riid, IID_IContextMenu)) {
        *ppv = (LPCONTEXTMENU)this;
    }
    if (*ppv) {
        AddRef();
        return NOERROR;
    }
    return E_NOINTERFACE;
}

STDMETHODIMP_(ULONG) ShellExtension::AddRef()
{
    return ++m_cRef;
}

STDMETHODIMP_(ULONG) ShellExtension::Release()
{
    if (--m_cRef)
        return m_cRef;
    delete this;
    return 0L;
}

STDMETHODIMP ShellExtension::Initialize(LPCITEMIDLIST pIDFolder, LPDATAOBJECT pDataObj, HKEY hRegKey)
{
    if (m_pDataObj)
        m_pDataObj->Release();
    if (pDataObj) {
        m_pDataObj = pDataObj;
        pDataObj->AddRef();
    }
    RegOpenKeyEx(HKEY_CURRENT_USER, szShellExtensionRegistryEntry, 0, KEY_READ | KEY_QUERY_VALUE, &m_hkey);
    return NOERROR;
}

STDMETHODIMP ShellExtension::GetCommandString(UINT_PTR idCmd, UINT uFlags, UINT FAR *reserved, LPSTR pszName, UINT cchMax)
{
/*!
 * This function is inherited from IContextMenu and should return some strings
 * lookup http://msdn.microsoft.com/en-us/library/bb776094(VS.85).aspx
 * It is called each time the user hovers over the context menu. 
 */
//    TCHAR szFileUserClickedOn[MAX_PATH];
//    DragQueryFile((HDROP)m_stgMedium.hGlobal, m_cbFiles - 1, szFileUserClickedOn, MAX_PATH);

    if (uFlags == GCS_HELPTEXT && cchMax > 35)
        lstrcpy((LPTSTR)pszName, szShellExtensionCommandString);
    return NOERROR;
}

