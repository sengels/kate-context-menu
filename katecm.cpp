//---------------------------------------------------------------------------
// Copyright 2012-2013 Patrick Spendrin <ps_ml@gmx.de>
//---------------------------------------------------------------------------

#include "commoncm.h"

#include <windows.h>

#include <sstream>

TCHAR szShellExtensionTitle[] = TEXT("Kate");
TCHAR szShellExtensionRegistryEntry[] = TEXT("Software\\kde.org\\contextmenu");
TCHAR szShellExtensionCommandString[] = TEXT("Edits the selected file(s) with the selected Kate session");
TCHAR szShellExtensionTitleBarError[] = TEXT("Kate Context Menu Extension");

STDMETHODIMP ShellExtension::QueryContextMenu(HMENU hMenu, UINT indexMenu, UINT idCmdFirst, UINT idCmdLast, UINT uFlags)
{
    UINT idCmd = idCmdFirst;

    HMENU submenu = CreatePopupMenu();

    std::vector<std::wstring> sessions = this->getKateSessions();

    InsertMenu(hMenu, indexMenu + 1, MF_BYPOSITION | MF_STRING, idCmd++, TEXT("Open with default Kate session"));
    if (m_hKateLogoBmp) {
        SetMenuItemBitmaps(hMenu, indexMenu + 1, MF_BYPOSITION, m_hKateLogoBmp, m_hKateLogoBmp);
    }

    for(std::vector<std::wstring>::iterator it = sessions.begin(); it != sessions.end(); it++) {
        InsertMenu(submenu, it - sessions.begin(), MF_BYPOSITION | MF_STRING, idCmd++, it->c_str());
    }

    // if you want to modify the menus according to the files selected here
    // use the following code
/*    TCHAR szFileUserClickedOn[MAX_PATH];

    FORMATETC fmte = {
        CF_HDROP,
        (DVTARGETDEVICE FAR *)NULL,
        DVASPECT_CONTENT,
        -1,
        TYMED_HGLOBAL
    };

    HRESULT hres = m_pDataObj->GetData(&fmte, &m_stgMedium);

    if (SUCCEEDED(hres)) {
        if (m_stgMedium.hGlobal)
            m_cbFiles = DragQueryFile((HDROP)m_stgMedium.hGlobal, (UINT)-1, 0, 0);
            for (unsigned i = 0; i < m_cbFiles; i++) {
                DragQueryFile((HDROP)m_stgMedium.hGlobal, i, szFileUserClickedOn, MAX_PATH);
                OutputDebugString(szFileUserClickedOn);
            }
    }*/

    InsertMenu(hMenu, indexMenu + 2, MF_POPUP | MF_BYPOSITION, (UINT_PTR)submenu, TEXT("Open with Kate session") );
    if (m_hKateLogoBmp) {
        SetMenuItemBitmaps(hMenu, indexMenu + 2, MF_BYPOSITION, m_hKateLogoBmp, m_hKateLogoBmp);
    }

    return ResultFromShort(idCmd - idCmdFirst);
}

STDMETHODIMP ShellExtension::InvokeCommand(LPCMINVOKECOMMANDINFO lpcmi)
{
    TCHAR   szFileUserClickedOn[MAX_PATH];

    int commandOffset;

    // check if we are called to handle something else
    // if the HIWORD is set, the lpVerb contains a string pointer with something like runAs or open which we don't want to handle
    if(lpcmi->cbSize == sizeof(CMINVOKECOMMANDINFOEX) &&
       (lpcmi->fMask & CMIC_MASK_UNICODE) != 0)
    {
        LPCMINVOKECOMMANDINFOEX lpcmiEx = (LPCMINVOKECOMMANDINFOEX)lpcmi;
        if (HIWORD(lpcmiEx->lpVerbW) == 0) {
            commandOffset = LOWORD(lpcmi->lpVerb);
        } else {
            return E_FAIL;
        }
    } else {
        if (HIWORD(lpcmi->lpVerb) == 0) {
            commandOffset = LOWORD(lpcmi->lpVerb);
        } else {
            return E_FAIL;
        }
    }

    // when clicking on the menu, you can use modifier keys here
    bool openNewSession = (lpcmi->fMask & CMIC_MASK_SHIFT_DOWN) != 0;

    FORMATETC fmte = {
        CF_HDROP,
        (DVTARGETDEVICE FAR *)NULL,
        DVASPECT_CONTENT,
        -1,
        TYMED_HGLOBAL
    };

    HRESULT hres = m_pDataObj->GetData(&fmte, &m_stgMedium);

    std::vector<std::wstring> fileList;

    if (SUCCEEDED(hres)) {
        if (m_stgMedium.hGlobal) {
            m_cbFiles = DragQueryFile((HDROP)m_stgMedium.hGlobal, (UINT)-1, 0, 0);
            for (unsigned i = 0; i < m_cbFiles; i++) {
                DragQueryFile((HDROP)m_stgMedium.hGlobal, i, szFileUserClickedOn, MAX_PATH);
                fileList.push_back(fromNativeSeparators(szFileUserClickedOn));
            }
        }
    }

    const std::wstring cmdLine = getKateCommandline(fileList, commandOffset - 1, openNewSession);

#if 0
    // for debugging only!
    return S_OK;
#endif

    STARTUPINFO si;
    PROCESS_INFORMATION pi;
    const int tmpLen = cmdLine.length() + 1;
    TCHAR* tmpCmd = new TCHAR[tmpLen];
    wcsncpy_s(tmpCmd, tmpLen, cmdLine.c_str(), tmpLen);
    ZeroMemory(&si, sizeof(si));
    si.cb = sizeof(si);
    si.dwFlags = STARTF_USESHOWWINDOW;
    si.wShowWindow = SW_RESTORE;
    if (!CreateProcess(NULL, tmpCmd, NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi)) {
        MessageBox(lpcmi->hwnd,
                   TEXT("Error creating process: Kate needs to be in the same directory or in the PATH"),
                   TEXT("Kate Context Menu Extension"),
                   MB_OK);
    }
    delete[] tmpCmd;
    return S_OK;
}

std::wstring ShellExtension::fromNativeSeparators(const std::wstring& path)
{
    std::wstring ret = path;
    size_t pos = ret.find(TEXT("\\"));
    while(pos != ret.npos) {
        ret.replace(pos, 1, TEXT("/"));
        pos = ret.find(TEXT("\\"));
    }
    return ret;
}

std::wstring ShellExtension::getKateCommandline(std::vector<std::wstring> fileList, int session, bool openNewSession) const
{
    std::wstring        katepath;
    std::wstringstream  cmd;
    std::string         dllName = CM_OUTPUT_NAME ".dll";
    TCHAR               dllPath[MAX_PATH];
#if 0
    TCHAR               debugParameters[1024];

    wsprintf(debugParameters, L"parameters for kate commandline: #files: %i, session: %i, open new session: %s\n", fileList.size(), session, (openNewSession ? L"yes" : L"no"));
    OutputDebugString(debugParameters);
#endif

    // it seems that for COM objects the hInstance from DllMain is wrong
    // thus we have to search for ourselves
    GetModuleFileName(GetModuleHandleA(dllName.c_str()), dllPath, MAX_PATH);

    katepath = dllPath;
    katepath.replace(katepath.rfind(TEXT("\\")) + 1, dllName.length(), TEXT(""));
    katepath.append(TEXT("kate.exe"));

    cmd << katepath;
    if(openNewSession) {
        cmd << " " << "-n";
    }

    std::vector<std::wstring> sessions = this->getKateSessions();

    if(session >= 0 && session < (int)sessions.size()) {
        cmd << " -s " << this->getKateSessions()[session];
    }

    for(unsigned i = 0; i < fileList.size(); i++) {
        cmd << " \"" << fileList[i].c_str() << "\"";
    }

#if 0
    OutputDebugString(L"generated kate commandline:");
    OutputDebugString(cmd.str().c_str());
#endif
    return cmd.str();
}

std::vector<std::wstring> ShellExtension::getKateSessions() const
{
    std::vector<std::wstring> ret;
    DWORD len = MAX_PATH;
    TCHAR* kdeconfigdir = new TCHAR[len + 1];
    len *= sizeof(TCHAR);
//    ret.push_back(TEXT("[default]"));
    if(ERROR_SUCCESS != RegQueryValueEx(m_hkey, TEXT("kdeconfigdir"), NULL, NULL, (BYTE*)kdeconfigdir, &len)) {
        SHGetFolderPath(NULL, CSIDL_APPDATA, NULL, 0, kdeconfigdir);
    }

    WIN32_FIND_DATA FindFileData;
    HANDLE hFind;

    std::wstring s = kdeconfigdir;
    s += TEXT("\\share\\apps\\kate\\sessions\\*.katesession");
    hFind = FindFirstFile(s.c_str(), &FindFileData);

    if(hFind == INVALID_HANDLE_VALUE) return ret;

    std::wstring sessionName(FindFileData.cFileName);
    // replace .katesession
    sessionName.replace(sessionName.end() - 12, sessionName.end(), TEXT(""));
    ret.push_back(sessionName);

    while(FindNextFile(hFind, &FindFileData)) {
        sessionName = FindFileData.cFileName;
        sessionName.replace(sessionName.end() - 12, sessionName.end(), TEXT(""));
        ret.push_back(sessionName);
    }
    FindClose(hFind);
    return ret;
}

