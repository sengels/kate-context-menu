#include <windows.h>
#define INITGUID
#include "commoncm.h"
#include <iostream>

#if defined(__MINGW32__) && !defined(MINGW_HAS_SECURE_API)
#define wcscpy_s(src,srcsize,dst) wcscpy(src,dst)
#endif

int main(int argc, char**argv) {
    std::cout << "trying to access contextmenu:" << std::endl;
    if(CoInitialize(NULL) != S_OK) {
        std::cout << "failed to initialize COM!" << std::endl;
        return -1;
    }
    
    COSERVERINFO si; 
    WCHAR wcsHost[64];
    ZeroMemory(&si, sizeof(si));
    wcscpy_s(wcsHost, 64, TEXT("\\\\localhost"));
    si.pwszName = wcsHost;
    MULTI_QI rgmqi[1];
    ZeroMemory(rgmqi, sizeof(rgmqi));
    rgmqi[0].pIID = &IID_IUnknown;
    ShellExtension* myExt = NULL;
    HRESULT hr = CoGetClassObject(CLSID_ShellExtension, CLSCTX_INPROC_SERVER, NULL, IID_IClassFactory, (void**)&myExt);
    std::cout << myExt << std::endl;
    if (hr == S_OK) {
//        pUnknown  = (IUnknown*)rgmqi[0].pItf;
    } else {
        LPVOID lpMsgBuf;
        DWORD dw = GetLastError();

        FormatMessageA(
        FORMAT_MESSAGE_ALLOCATE_BUFFER | 
        FORMAT_MESSAGE_FROM_SYSTEM |
        FORMAT_MESSAGE_IGNORE_INSERTS,
        NULL,
        dw,
        MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
        (LPSTR) &lpMsgBuf,
        0, NULL );
        std::cout << "CoCreateInstanceEx failed." << hr << " " << REGDB_E_CLASSNOTREG << " "  << CLASS_E_NOAGGREGATION << " "  << CO_S_NOTALLINTERFACES << " "  << E_NOINTERFACE << " " << E_INVALIDARG << std::endl;
        std::cout << (CHAR*)lpMsgBuf << std::endl;
        LocalFree(lpMsgBuf);
        return -1;
    }

    std::cout << "uninitializing COM" << std::endl;
    CoUninitialize();
    return 0;
}
